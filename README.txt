# CKeditor Add Class Plugin

## Description

This plugin provide feature to add class to current element using dropdown (available
in context menu and dropdown field values are configurable from text format ).

## Usage

Choose class values from dropdown available from context menu. It will add class to the current tag.

## Installation

This module requires:

- `drupal/ckeditor`: Drupal core module _CKEditor_.

### Install using Composer (recommended)

If you use Composer to manage dependencies, edit `/composer.json` as follows.

- Run `composer require --prefer-dist composer/installers` to ensure that you have the `composer/installers` package installed. This package facilitates the installation of packages into directories other than `/vendor` (e.g. `/libraries`) using Composer.

- Add one of the following to the "installer-paths" section of `composer.json`, or alter as necessary:

```json
"libraries/{$name}": ["type:drupal-library"],
```

_or_

```json
"libraries/ckeditor/plugins/{$name}": ["type:drupal-library"],
```

- Add the following to the "repositories" section of `composer.json`:

```json
{
  "type": "package",
  "package": {
    "name": "ckeditor-plugin/ck_add_class",
    "version": "1.0.1",
    "type": "drupal-library",
    "dist": {
      "url": "https://github.com/ashishsingh1331/ck_add_class/archive/refs/tags/1.0.1.zip",
      "type": "zip"
    }
  }
}
```

- Run `composer require --prefer-dist 'ckeditor-plugin/ck_add_class:1.0.1'` - you should find that new directory has been created under `/libraries`

- Then install this module: `composer require 'drupal/ckeditor_add_class'`

### Install manually

- Open https://github.com/ashishsingh1331/ck_add_class and download  zip file (https://github.com/ashishsingh1331/ck_add_class/archive/refs/tags/1.0.1.zip)_.

- Extract the content and copy to _libraries_ folder. i.e. `/libraries/ck_add_class/plugin.js`

- Download [CKEditor Add Class](https://www.drupal.org/project/ckeditor_add_class) (this module) and then extract files into `/modules/contrib/ckeditor_add_class`

## Resources

[Other contributed modules and plug-ins available for CKEditor](https://www.drupal.org/documentation/modules/ckeditor/contrib)

## Maintainers

Ashish Singh - https://www.drupal.org/u/drupalbabaji
